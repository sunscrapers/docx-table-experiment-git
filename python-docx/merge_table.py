from docx import Document

DATA = [
    ['10', '1337-ABC', 'Milk'],
    ['5', '7331-XYZ', 'Bread'],
    ['25', '5151-QWE', 'Apple'],
]

document = Document('python-docx/merge_table.docx')

table = document.tables[0]

# we could do some manual merging here, now i'm only adding rows here

for vals in DATA:
    row_cells = table.add_row().cells
    for cell, val in zip(row_cells, vals):
        cell.text = val

document.save('output/python-docx_merge_table.docx')