from docx import Document

HEADER = ['Qty', 'Id', 'Desc']

DATA = [
    ['10', '1337-ABC', 'Milk'],
    ['5', '7331-XYZ', 'Bread'],
    ['25', '5151-QWE', 'Apple'],
]

COLUMN_WIDTH = 50

document = Document()

table = document.add_table(rows=1, cols=3, style='Light Grid Accent 5')

for cell, val in zip(table.rows[0].cells, HEADER):
    cell.text = val
    cell.width = COLUMN_WIDTH

for vals in DATA:
    row_cells = table.add_row().cells
    for cell, val in zip(row_cells, vals):
        cell.text = val

document.save('output/python-docx_create_table.docx')