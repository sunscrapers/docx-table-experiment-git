from mailmerge import MailMerge
from os import path

DATA = [
    ['10', '1337-ABC', 'Milk'],
    ['5', '7331-XYZ', 'Bread'],
    ['25', '5151-QWE', 'Apple'],
]

document = MailMerge(path.join(path.dirname(__file__), 'merge_table.docx'))

document.merge_rows('qty', [{'qty': row[0], 'id': row[1], 'desc': row[2]} for row in DATA])

with open('output/docx-mailmerge_merge_table.docx', 'w') as outfile:
    document.write(outfile)