To run experiment execute e.g.:

    $ python aspose/create_table.py
    
... and then check result in `output` directory.


But first of all install requirements:

    $ pip install -r requirements.txt