import os
import jpype

HEADER = ['Qty', 'Id', 'Desc']

DATA = [
    ['10', '1337-ABC', 'Milk'],
    ['5', '7331-XYZ', 'Bread'],
    ['25', '5151-QWE', 'Apple'],
]

COLUMN_WIDTH = 50

jarpath = os.path.join("/usr/share/java")
jpype.startJVM(jpype.getDefaultJVMPath(), "-Djava.ext.dirs=%s" % jarpath)
License = jpype.JClass("com.aspose.words.License")
CellFormat = jpype.JClass("com.aspose.words.CellFormat")
license = License()
license.setLicense("Aspose.Words.lic")
Document = jpype.JClass("com.aspose.words.Document")
DocumentBuilder = jpype.JClass("com.aspose.words.DocumentBuilder")
doc = Document()
builder = DocumentBuilder(doc)

table = builder.startTable()
table.StyleIdentifier = 249

# below lines rise error:
# *** RuntimeError: No matching overloads found. at src/native/common/jp_method.cpp:121
# cell_format = builder.getCellFormat()
# cell_format.setWidth(COLUMN_WIDTH)

for val in HEADER:
    builder.insertCell()
    builder.write(val)

builder.endRow()

for vals in DATA:
    for val in vals:
        builder.insertCell()
        builder.write(val)
    builder.endRow()


builder.endTable()

doc.save('output/aspose_create_table.docx')